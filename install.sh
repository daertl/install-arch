#!/usr/bin/env bash

. "./lib.sh"

packages=(
    #xdg
    #ntp
    #numlock
    #st
    #misc-packages
    #shell-themes
    #libreoffice
    #docker
    #rofi
    #teamviewer
    #texlive
    #vim
    nvim
    keepassxc
    dev-packages
    signal-desktop
    xournalapp
    zsh
    oh-my-zsh
    plugged
    yay
    brave-bin
    spotify
)
ensure_privileges

# upgrade system
log "updating system"
sudo pacman --sync --refresh --sysupgrade --needed --noconfirm 1> /dev/null

for package in "${packages[@]}"; do
    log "$package"
    ./"$package"/.install.sh
done

log "FINISHED INSTALLING"

log "Now, create these symbolic links: ~/Wallpaper, /Dev"

