#!/usr/bin/env bash

arch_packages=(
    openssh
    make clang gdb
    git
    intellij-idea-community-edition
    java-runtime-common
    java-environment-common
    java11-openjfx
    jdk-openjdk
    jre-openjdk
    json-c
    json-glib
    kde-cli-tools
    kde-dev-scripts
    kde-gtk-config
    kdebugsettings
    kdeclarative
    kdeconnect
    make
    mariadb
    nasm
    neovim
    neovim-gtk
    nmap
    nextcloud-client
    networkmanager
    npm
    okular
    openssh
    openvpn
    p11-kit
    php
    python
    pycharm-community-edition
    python2
    stow
    wireshark-cli wireshark-qt
    filezilla
    maven
    flameshot

    #teams

    #xarchiver zip unrar p7zip
    #the_silver_searcher
    #jq
    #shellcheck
    #composer
    #inkscape
    #dbeaver
    #yarn
    #ctags
    #code
    #prettier
    #valgrind
    #pandoc pandoc-crossref
    #go
    #httpie # curl for humans
)

aur_packages=(
    git-extras
    fzf-git
    fzf

    #insomnia
    #dragon-drag-and-drop
    #robo3t-bin
    #nodejs-live-server
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done

