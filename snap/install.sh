#!/usr/bin/env bash

# Install packages
sudo pacman --sync --needed --noconfirm snapd 1> /dev/null

# setup snap
sudo systemctl enable --now snapd.socket
sudo ln --symbolic /var/lib/snapd/snap /

# only keep one old version
sudo snap set system refresh.retain=2

